import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit{

  constructor(public navCtrl: NavController, private navegador: InAppBrowser) {
    
  }

  openLink(){
    this.navegador.create("https://www.jaksoluciones.com/","_system");
  }
  
  ngOnInit() {
    this.openLink();
  }

}
